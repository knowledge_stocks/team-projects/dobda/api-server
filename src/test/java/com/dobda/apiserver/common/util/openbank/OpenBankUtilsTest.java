package com.dobda.apiserver.common.util.openbank;

//import static org.junit.jupiter.api.Assertions.assertNotNull;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//@SpringBootTest
//class OpenBankUtilsTest {
//
//    @Test
//    @DisplayName("이용기관 토큰 테스트")
//    void getOrgToken() {
//        OrgTokenResponse tokenResponse = OpenBankUtils.getOrgToken();
//        String token = tokenResponse.getAccessToken();
//        RevokeTokenResponse revokeResponse = OpenBankUtils.revokeToken(token);
//
//        assertNotNull(token != null);
//        assertTrue("O0000".equals(revokeResponse.getRspCode()));
//    }
//
//    @Test
//    @DisplayName("사용자 토큰 갱신 테스트")
//    void refreshUserToken() {
//        RefreshTokenRequest refreshRequest = RefreshTokenRequest.builder()
//                .scope("login transfer")
//                .refreshToken("")
//                .build();
//        RefreshTokenResponse tokenResponse = OpenBankUtils.refreshUserToken(refreshRequest);
//        String token = tokenResponse.getAccessToken();
//
//        assertNotNull(token != null);
//    }
//}