package com.dobda.apiserver.security;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum MemberRoles {
    ADMIN(1, "ADMIN"),
    USER(2, "USER");

    private static final Map<Integer, MemberRoles> intToEnum =
            Arrays.stream(values()).collect(Collectors.toMap(MemberRoles::getCode, e -> e));

    @Getter
    @JsonValue
    private final Integer code;

    @Getter
    private final String role;

    @Getter
    private final String fullRole;

    private MemberRoles(int code, String role)
    {
        this.code = code;
        this.role = role;
        this.fullRole = "ROLE_" + role;
    }

    public static MemberRoles fromCode(int code) {
        MemberRoles result = intToEnum.get(code);
        return result;
    }
}
