package com.dobda.apiserver.security.config;

import com.dobda.apiserver.config.DobdaProperties;
import com.dobda.apiserver.security.authprovider.AdminAuthenticationProvider;
import com.dobda.apiserver.security.authprovider.UserAuthenticationProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@Configuration
public class SecurityBeans {

    @Bean
    public AuthenticationProvider memberUserAuthProvider() {
        return new UserAuthenticationProvider();
    }

    @Bean
    public AuthenticationProvider memberAdminAuthProvider() {
        return new AdminAuthenticationProvider();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();

        List<String> origins = DobdaProperties.getInstance().getAllowOrigins();
        // 프론트엔드의 주소를 여기에 추가해주어야 통신이 가능하다.
        for (String origin: origins) {
            configuration.addAllowedOrigin(origin);
        }
        configuration.addAllowedMethod("*");
        configuration.addAllowedHeader("*");
        configuration.setAllowCredentials(true);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
