package com.dobda.apiserver.api.review;

import com.dobda.apiserver.api.common.errorhandling.BaseException;
import com.dobda.apiserver.api.review.dto.AddReviewDto;
import com.dobda.apiserver.api.review.dto.ViewReviewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class ReviewController {
	
	@Autowired
	private ReviewService reviewService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
	
	// 후기 등록 //
	@PostMapping("/api/user/review")
	public ResponseEntity addReview(
            Authentication authentication,
			@ModelAttribute @Valid AddReviewDto addDto) {
		try {
            ViewReviewDto result = reviewService.addRequesterReview((Long) authentication.getPrincipal(), addDto);
            if(result.getTargetInfo() != null) {
                simpMessagingTemplate.convertAndSend(
                        "/ws/topic/" + result.getTargetInfo().getId() + "/noti", "");
            }
            return new ResponseEntity(result, HttpStatus.CREATED);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
	}
	
	
//	// 주문 id로 후기 조회 //
//	@GetMapping(value= {"/api/user/reviews/{orderId}", "/api/helper/reviews/{orderId}"})
//	public ResponseEntity getReviewFromOrder(@PathVariable("orderId") BigInteger orderId) {
//		try {
//			ViewReviewDto review = reviewService.getReviewFromOrder(orderId);
//			return new ResponseEntity(review, HttpStatus.OK);
//
//		}catch (BaseException e) {
//			return new ResponseEntity(e.getMessage(), e.getStatusCode());
//		}
//	}
//
//
//	// 작성 후기 목록 조회 //
//	@GetMapping("/api/user/reviews")
//	public ResponseEntity getUserReviewList(
//			Authentication authentication,
//			 @RequestParam(value="page", required = false) Integer page) {
//		try {
//			Page<ViewReviewDto> reviews = reviewService.getUserReviewList(page, (Long)authentication.getPrincipal());
//			return new ResponseEntity(reviews.getContent(), HttpStatus.OK);
//		}catch(BaseException e) {
//			return new ResponseEntity(e.getMessage(), e.getStatusCode());
//		}
//	}
//
//
//	// 받은 후기 목록 조회 (헬퍼) //
//	@GetMapping("/api/helper/reviews")
//	public ResponseEntity getHelperReviewList(Authentication authentication,
//			@RequestParam(value="page", required = false) Integer page) {
//		try {
//			Page<ViewReviewDto> reviews = reviewService.getHelperReviewList(page, (Long)authentication.getPrincipal());
//			return new ResponseEntity(reviews.getContent(), HttpStatus.OK);
//		}catch(BaseException e) {
//			return new ResponseEntity(e.getMessage(), e.getStatusCode());
//		}
//	}
//
//
//	// 후기 단건 조회 //
//	@GetMapping(value = {"/api/user/reviews/{reviewId}", "/api/helper/reviews/{reviewId}"})
//	public ResponseEntity getReview(@PathVariable("reviewId") BigInteger reviewId) {
//		try {
//			ViewReviewDto review = reviewService.getReview(reviewId);
//			return new ResponseEntity(review, HttpStatus.OK);
//		}catch(BaseException e) {
//			return new ResponseEntity(e.getMessage(), e.getStatusCode());
//		}
//	}
//
//
//	// 후기 삭제 //
//	@DeleteMapping("/api/user/review/delete")
//	public ResponseEntity deleteReview(@RequestParam("reviewId") BigInteger id) {
//		try {
//            reviewService.deleteReview(id);
//            return new ResponseEntity(HttpStatus.OK);
//        } catch (BaseException e) {
//            return new ResponseEntity(e.getMessage(), e.getStatusCode());
//        }
//	}

	
	
}
