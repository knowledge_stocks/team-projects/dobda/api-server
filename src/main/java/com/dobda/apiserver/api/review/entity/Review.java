package com.dobda.apiserver.api.review.entity;

import com.dobda.apiserver.api.member.user.entity.MemberUser;
import com.dobda.apiserver.api.order.entity.Order;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigInteger;
import java.time.ZonedDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@DynamicInsert
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"order_id", "writer_id"})
})
public class Review {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "BIGINT UNSIGNED")
	private BigInteger id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id", nullable = false)
    private Order order;

	// 작성자 ID
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "writer_id")
	private MemberUser writer;

    // 작성자 대상자 ID
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "target_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private MemberUser target;
		
	// 후기 내용
    @Size(max = 200)
	@Column(nullable = false)
	private String content;

	// 후기 평가
	@Column(columnDefinition = "TINYINT UNSIGNED", nullable = false)
	private Integer evaluation;

	// 작성 일 
    @CreatedDate
    @Column(columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP", nullable = false)
    private ZonedDateTime regTime;

    @Builder
    public Review(Order order, MemberUser writer, MemberUser target, String content, Integer evaluation) {
        this.order = order;
        this.writer = writer;
        this.target = target;
        this.content = content;
        this.evaluation = evaluation;
    }
}
