package com.dobda.apiserver.api.review;

import com.dobda.apiserver.api.common.errorhandling.BadRequestException;
import com.dobda.apiserver.api.common.errorhandling.ForbiddenException;
import com.dobda.apiserver.api.common.errorhandling.NotFoundException;
import com.dobda.apiserver.api.member.helper.entity.MemberHelper;
import com.dobda.apiserver.api.member.user.NotificationMemberRepository;
import com.dobda.apiserver.api.member.user.entity.MemberUser;
import com.dobda.apiserver.api.member.user.entity.NotificationMember;
import com.dobda.apiserver.api.order.OrderRepository;
import com.dobda.apiserver.api.order.entity.Order;
import com.dobda.apiserver.api.review.dto.AddReviewDto;
import com.dobda.apiserver.api.review.dto.ViewReviewDto;
import com.dobda.apiserver.api.review.entity.Review;
import com.dobda.apiserver.common.enums.OrderState;
import com.dobda.apiserver.common.enums.ResourceType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ReviewService {
	
	private final ReviewRepository reviewRepository;
	private final OrderRepository orderRepository;
    private final EntityManager em;
    private final NotificationMemberRepository notificationMemberRepository;

	private static final int size = 25;
	
	// 후기 등록 //
	@Transactional
	public ViewReviewDto addRequesterReview(Long writerId, AddReviewDto addDto) {
        Review findReview = reviewRepository.findByWriter_IdAndOrder_Id(writerId, addDto.getOrderId());
        if(findReview != null) {
            throw new BadRequestException("이미 남긴 리뷰가 있습니다.");
        }
		Optional<Order> findOrder = orderRepository.findById(addDto.getOrderId());
		if(findOrder.isEmpty()) {
			throw new NotFoundException("주문 정보를 찾을 수 없습니다.");
		}
        Order order = findOrder.get();
        if(order.getState() != OrderState.COMPLETE &&
            order.getState() != OrderState.WAITING_CALC
        ) {
            throw new BadRequestException("리뷰는 주문이 완료된 후에 작성할 수 있습니다.");
        }
        MemberUser requester = order.getMemberUser();
        MemberHelper helper = order.getMemberHelper();

        MemberUser writer = null;
        MemberUser target = null;
        boolean isTargetHelper = false;
        if(requester != null && requester.getId().equals(writerId)) {
            isTargetHelper = true;
            writer = requester;
            if(helper != null) {
                target = helper.getMemberUser();
            }
        } else if(helper != null && helper.getId().equals(writerId)) {
            writer = helper.getMemberUser();
            target = requester;
        } else {
            throw new ForbiddenException("권한이 없습니다.");
        }

        if(target != null) {
            double point = target.getTrustPoint();
            int evaluation = addDto.getEvaluation()-3;
            point += evaluation / 100.;
            target.setTrustPoint(point);
            if(isTargetHelper && evaluation > 0) {
                helper.setTotalSuccessCnt(helper.getTotalSuccessCnt() + 1);
            }
        }

		Review review = Review.builder()
				.order(order)
				.writer(writer)
				.target(target)
				.content(addDto.getContent())
				.evaluation(addDto.getEvaluation())
				.build();

		reviewRepository.save(review);

        if(target != null) {
            NotificationMember newNoti = NotificationMember.builder()
                    .memberUser(target)
                    .message("'" + order.getRequest().getTitle() + "' 의뢰의 상대방이 후기를 남기셨어요.")
                    .targetType(ResourceType.REVIEW)
                    .targetId(review.getId())
                    .build();
            notificationMemberRepository.save(newNoti);
        }
        notificationMemberRepository.flush();
        em.clear();
        review = reviewRepository.findById(review.getId()).get();

		return ViewReviewDto.fromEntity(review);
	}
	
	
//	// 주문 id로 후기 조회 //
//	public ViewReviewDto getReviewFromOrder(BigInteger orderId) {
//		Optional<Review> findReview = reviewRepository.findByOrder_Id(orderId);
//		if(findReview.isEmpty()) {throw new NotFoundException("해당 거래 내역의 후기가 없습니다.");}
//
//		return ViewReviewDto.fromEntity(findReview.get());
//	}
//
//
//	// 작성 후기 목록 조회 //
//	public Page<ViewReviewDto> getUserReviewList(Integer page, Long id){
//		int pageNum = 0;
//		if(page != null && page > 0) {pageNum = page - 1;}
//
//		Pageable pageable = PageRequest.of(pageNum, size, Sort.Direction.DESC, "regTime");
//		Page<Review> findReviews = reviewRepository.findByWriter_Id(id, pageable);
//
//		if(findReviews.isEmpty()) {throw new BadRequestException("작성하신 후기가 없습니다.");}
//
//		return new PageImpl<>(
//				findReviews.getContent().stream()
//				.map(ViewReviewDto::fromEntity)
//				.collect(Collectors.toList())
//				, pageable, findReviews.getTotalElements());
//	}
//
//
//	// 받은 후기 목록 조회 //
//	public Page<ViewReviewDto> getHelperReviewList(Integer page, Long id){
//		int pageNum = 0;
//		if(page != null && page > 0) {pageNum = page - 1;}
//
//		Pageable pageable = PageRequest.of(pageNum, size, Sort.Direction.DESC, "regTime");
//		Page<Review> findReviews = reviewRepository.findByTarget_Id(id, pageable);
//
//		if(findReviews.isEmpty()) {throw new BadRequestException("받은 후기가 없습니다.");}
//
//		return new PageImpl<>(
//				findReviews.getContent().stream()
//				.map(ViewReviewDto::fromEntity)
//				.collect(Collectors.toList())
//				,pageable, findReviews.getTotalElements());
//	 }
//
//
//
//	// 후기 단건 조회 //
//	public ViewReviewDto getReview(BigInteger id) {
//		Optional<Review> findReview = reviewRepository.findById(id);
//		if(findReview.isEmpty()) {
//			throw new NotFoundException("해당 후기를 찾을 수 없습니다.");
//		}
//		return ViewReviewDto.fromEntity(findReview.get());
//	}
//
//
//	// 후기 삭제 //
//	@Transactional
//	public void deleteReview(BigInteger id) {
//		Optional<Review> findReview = reviewRepository.findById(id);
//		if(findReview.isEmpty()) {
//			throw new NotFoundException("해당 후기가 없습니다.");
//		}
//		reviewRepository.deleteById(id);
//	}
}
