package com.dobda.apiserver.api.review.dto;

import com.dobda.apiserver.api.member.user.dto.MemberPublicInfo;
import com.dobda.apiserver.api.review.entity.Review;
import lombok.Builder;
import lombok.Data;

import java.math.BigInteger;
import java.time.ZonedDateTime;

@Data
@Builder
public class ViewReviewDto {
	
	private BigInteger id;
	private BigInteger orderId;
	private MemberPublicInfo writerInfo;
	private MemberPublicInfo targetInfo;
	private String content;
	private Integer evaluation;
	private ZonedDateTime regTime;
	
	public static ViewReviewDto fromEntity(Review entity) {
        if(entity == null) {return null;}

        return builder()
                .id(entity.getId())
                .orderId(entity.getOrder() != null
                        ? entity.getOrder().getId()
                        : null)
                .writerInfo(MemberPublicInfo.fromEntity(entity.getWriter()))
                .targetInfo(MemberPublicInfo.fromEntity(entity.getTarget()))
                .content(entity.getContent())
                .evaluation(entity.getEvaluation())
                .regTime(entity.getRegTime())
                .build();
	}	
}
