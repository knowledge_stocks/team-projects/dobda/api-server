package com.dobda.apiserver.api.requestboard.service;

import com.dobda.apiserver.api.common.errorhandling.*;
import com.dobda.apiserver.api.member.helper.MemberHelperRepository;
import com.dobda.apiserver.api.member.helper.entity.MemberHelper;
import com.dobda.apiserver.api.member.user.MemberUserRepository;
import com.dobda.apiserver.api.member.user.NotificationMemberRepository;
import com.dobda.apiserver.api.member.user.entity.MemberUser;
import com.dobda.apiserver.api.member.user.entity.NotificationMember;
import com.dobda.apiserver.api.order.OrderRepository;
import com.dobda.apiserver.api.order.entity.Order;
import com.dobda.apiserver.api.requestboard.dto.RequestApplyAddDto;
import com.dobda.apiserver.api.requestboard.dto.RequestApplyChooseDto;
import com.dobda.apiserver.api.requestboard.dto.RequestApplyInfo;
import com.dobda.apiserver.api.requestboard.dto.RequestApplyModDto;
import com.dobda.apiserver.api.requestboard.entity.ReqApply;
import com.dobda.apiserver.api.requestboard.entity.Request;
import com.dobda.apiserver.api.requestboard.repository.ReqApplyRepository;
import com.dobda.apiserver.api.requestboard.repository.RequestRepository;
import com.dobda.apiserver.common.enums.PaymentMethod;
import com.dobda.apiserver.common.enums.ReqAplyState;
import com.dobda.apiserver.common.enums.RequestState;
import com.dobda.apiserver.common.enums.ResourceType;
import com.dobda.apiserver.common.util.openbank.OpenBankUtils;
import com.dobda.apiserver.common.util.openbank.payment.WithdrawResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ReqApplyService {

	private final Logger logger = LoggerFactory.getLogger(RequestService.class);

	private final ReqApplyRepository reqApplyRepository;
	private final RequestRepository requestRepository;
	private final MemberHelperRepository memberHelperRepository;
	private final MemberUserRepository memberUserRepository;
    private final NotificationMemberRepository notificationMemberRepository;
    private final OrderRepository orderRepository;

    private final EntityManager em;

    /*
     * 의뢰신청
     */
    @Transactional
    public RequestApplyInfo applyToRequest(Long helperId, RequestApplyAddDto applyDto) {
        Optional<MemberHelper> findHelper = memberHelperRepository.findById(helperId);
        if(findHelper.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberHelper helper = findHelper.get();

        Optional<Request> findRequest = requestRepository.findById(applyDto.getRequestId());
        if(findRequest.isEmpty()) {
            throw new NotFoundException("의뢰글 정보를 찾을 수 없습니다.");
        }
        Request request = findRequest.get();
        if(request.getState() != RequestState.POSTED) {
            throw new BadRequestException("의뢰글이 현재 모집중이 아닙니다.");
        }
        if(!request.getIsCanSuggestPayment() && request.getPayment() < applyDto.getAmount()) {
            throw new BadRequestException("의뢰인이 제안한 금액보다 높은 금액을 제안할 수 없습니다.");
        }

        ReqApply reqApply = reqApplyRepository.findByMemberHelper_IdAndRequest_Id(helperId, applyDto.getRequestId());
        if(reqApply != null) {
            return null; // 이미 작성한 제안이 있는 경우
        }

		reqApply = ReqApply.builder()
				.memberHelper(helper)
				.request(request)
				.amount(applyDto.getAmount())
                .description(applyDto.getDescription())
                .build();
		reqApplyRepository.save(reqApply);

        if(request.getMemberUser() != null) {
            NotificationMember newNoti = NotificationMember.builder()
                    .memberUser(request.getMemberUser())
                    .message("'" + request.getTitle() + "' 의뢰글에 새로운 제안이 들어왔습니다.")
                    .targetType(ResourceType.REQUEST)
                    .targetId(request.getId())
                    .build();
            notificationMemberRepository.save(newNoti);
        }

        reqApplyRepository.flush();
        em.clear();
        reqApply = reqApplyRepository.findById(reqApply.getId()).get();

        return RequestApplyInfo.fromEntity(reqApply);
    }

	/*
	 * 의뢰신청 변경
	 * 희망보수, 상세내용, 시간만 변경 가능
	 * */
	@Transactional
	public RequestApplyInfo updateReqApply(Long helperId, RequestApplyModDto upDto) {
        Optional<MemberHelper> findHelper = memberHelperRepository.findById(helperId);
        if(findHelper.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberHelper helper = findHelper.get();

        Optional<Request> findRequest = requestRepository.findById(upDto.getRequestId());
        if(findRequest.isEmpty()) {
            throw new NotFoundException("의뢰글 정보를 찾을 수 없습니다.");
        }
        Request request = findRequest.get();
        if(request.getState() != RequestState.POSTED) {
            throw new BadRequestException("의뢰글이 현재 모집중이 아닙니다.");
        }
        if(!request.getIsCanSuggestPayment() && request.getPayment() < upDto.getAmount()) {
            throw new BadRequestException("의뢰인이 제안한 금액보다 높은 금액을 제안할 수 없습니다.");
        }

        ReqApply reqApply = reqApplyRepository.findByMemberHelper_IdAndRequest_Id(helperId, upDto.getRequestId());
        if(reqApply == null) {
            throw new NotFoundException("제안 정보를 찾을 수 없습니다.");
        }
        reqApply.setDescription(upDto.getDescription());
        reqApply.setAmount(upDto.getAmount());
        reqApply.setState(ReqAplyState.WAITING);

        reqApplyRepository.flush();
        em.clear();
        reqApply = reqApplyRepository.findById(reqApply.getId()).get();

        return RequestApplyInfo.fromEntity(reqApply);
	}

	/*
	 * 의뢰 수락 또는 거절 true = 수락 false = 거절
	 */
	@Transactional()
	public NotificationMember chooseReqApply(Long memberId, BigInteger applyId, RequestApplyChooseDto dto) {
        Optional<ReqApply> findApply = reqApplyRepository.findById(applyId);
        if(findApply.isEmpty()) {
            throw new NotFoundException("항목을 찾을 수 없습니다.");
        }
        ReqApply apply = findApply.get();
        if(apply.getState() != ReqAplyState.WAITING) {
            throw new BadRequestException("이미 승인되거나 거부된 제안입니다.");
        }
        Request request = apply.getRequest();
        MemberUser requester = request.getMemberUser();
        if(requester == null || !requester.getId().equals(memberId)) {
            throw new ForbiddenException("권한이 없습니다.");
        }
        if(request.getState() == RequestState.APPLIED) {
            throw new BadRequestException("이미 진행중인 거래가 있습니다.");
        }
        if(request.getState() == RequestState.DELETED) {
            throw new BadRequestException("삭제된 의뢰글입니다.");
        }
        if(request.getState() == RequestState.BLOCK) {
            throw new BadRequestException("제재된 의뢰글입니다.");
        }
        request.setState(RequestState.APPLIED);
        apply.setState(ReqAplyState.SELECTED);

        MemberHelper helper = apply.getMemberHelper();

        PaymentMethod paymentMethod = PaymentMethod.fromCode(dto.getPayMethod());
        if(paymentMethod == PaymentMethod.ACCOUNT_TRANSFER) {
            if(requester.getBankToken() == null) {
                throw new UnauthorizedException("오픈뱅킹 인증이 필요합니다. 인증 정보를 등록해주세요");
            }
            WithdrawResponse response = OpenBankUtils.withdraw(
                    requester.getBankToken(), dto.getFintech(), dto.getBankName(),
                    requester.getBankUserCode(), apply.getAmount());
            if(!response.getRsp_code().equals("A0000")) {
                throw new InternalServerErrorException(
                        "계좌에서 금액을 인출하지 못했습니다. 선택하신 계좌가 올바른지 확인해주세요. 에러 메시지: " + response.getRsp_message());
            }
        }

        Order newOrder = Order.builder()
                .memberUser(requester)
                .memberHelper(helper)
                .request(request)
                .requesterFintech(dto.getFintech())
                .requesterBankName(dto.getBankName())
                .payMethod(PaymentMethod.fromCode(dto.getPayMethod()))
                .amount(apply.getAmount())
                .build();

        orderRepository.save(newOrder);

        NotificationMember newNoti = NotificationMember.builder()
                .memberUser(helper.getMemberUser())
                .message("'" + request.getTitle() + "' 의뢰글의 제안이 수락되었습니다.")
                .targetType(ResourceType.REQUEST)
                .targetId(request.getId())
                .build();
        notificationMemberRepository.save(newNoti);

        return newNoti;
	}

    @Transactional
    public NotificationMember rejectReqApply(Long memberId, BigInteger applyId) {
        Optional<ReqApply> findApply = reqApplyRepository.findById(applyId);
        if(findApply.isEmpty()) {
            throw new NotFoundException("항목을 찾을 수 없습니다.");
        }
        ReqApply apply = findApply.get();
        if(apply.getState() != ReqAplyState.WAITING) {
            throw new BadRequestException("이미 승인되거나 거부된 제안입니다.");
        }
        Request request = apply.getRequest();
        MemberUser requester = request.getMemberUser();
        if(requester == null || !requester.getId().equals(memberId)) {
            throw new ForbiddenException("권한이 없습니다.");
        }
        if(request.getState() == RequestState.DELETED) {
            throw new BadRequestException("삭제된 의뢰글입니다.");
        }
        if(request.getState() == RequestState.BLOCK) {
            throw new BadRequestException("제재된 의뢰글입니다.");
        }
        apply.setState(ReqAplyState.REJECTED);

        MemberHelper helper = apply.getMemberHelper();

        NotificationMember newNoti = NotificationMember.builder()
                .memberUser(helper.getMemberUser())
                .message("'" + request.getTitle() + "' 의뢰글의 제안이 거부되었습니다.")
                .targetType(ResourceType.REQUEST)
                .targetId(request.getId())
                .build();
        notificationMemberRepository.save(newNoti);

        return newNoti;
    }

    @Transactional
    public void deleteReqApply(Long helperId, BigInteger applyId) {
        Optional<ReqApply> findApply = reqApplyRepository.findById(applyId);
        if(findApply.isEmpty()) {
            throw new NotFoundException("항목을 찾을 수 없습니다.");
        }
        ReqApply apply = findApply.get();
        MemberHelper helper = apply.getMemberHelper();
        if(helper == null || !helper.getId().equals(helperId)) {
            throw new ForbiddenException("권한이 없습니다.");
        }
        reqApplyRepository.delete(apply);
    }

	/*
	 * 의뢰수락 상태 조회
	 */
	public RequestApplyInfo viewReqApply(BigInteger reqApplyId) {
		Optional<ReqApply> findApply = reqApplyRepository.findById(reqApplyId);
        if(findApply.isEmpty()) {
            throw new NotFoundException("항목을 찾을 수 없습니다.");
        }
		return RequestApplyInfo.fromEntity(findApply.get());
	}

	/*
	 * 사용자가 지원한 의뢰 리스트 조회
	 */
    public Page<RequestApplyInfo> findApplyList(Integer page, BigInteger requestId, Long memberId) {
        Optional<Request> findRequest = requestRepository.findById(requestId);
        if(findRequest.isEmpty()) {
            throw new NotFoundException("의뢰글 정보를 찾을 수 없습니다.");
        }

        int pageNum = 0;
        if (page != null) {
            pageNum = page - 1;
        }
        if (pageNum < 0) {
            pageNum = 0;
        }
        Pageable pageable = PageRequest.of(pageNum, 25, Sort.Direction.ASC, "modTime");
        Page<ReqApply> getResults = null;
        if(memberId == null) { // 관리자일 때
            getResults = reqApplyRepository.findByRequest_IdAndState(requestId, ReqAplyState.WAITING, pageable);
        } else { // 일반 멤버일 때
            getResults = reqApplyRepository.findByMemberHelper_IdNotAndRequest_IdAndState(memberId, requestId, ReqAplyState.WAITING, pageable);
            if(pageNum == 0) {
                ReqApply reqApply = reqApplyRepository.findByMemberHelper_IdAndRequest_Id(memberId, requestId);
                if(reqApply != null) {
                    List<ReqApply> newList = new ArrayList<>(getResults.getContent());
                    newList.add(0, reqApply);
                    pageable = PageRequest.of(pageNum, 26, Sort.Direction.ASC, "modTime");

                    return new PageImpl(
                            newList.stream()
                                    .map(RequestApplyInfo::fromEntity)
                                    .collect(Collectors.toList()),
                            pageable, getResults.getTotalElements() + 1);
                }
            }
        }

        return new PageImpl(
                getResults.getContent().stream()
                        .map(RequestApplyInfo::fromEntity)
                        .collect(Collectors.toList()),
                pageable, getResults.getTotalElements());
    }

    public Page<RequestApplyInfo> myAcceptApplyList(Integer page, Long memberId) {
        Optional<MemberUser> memberOption = memberUserRepository.findById(memberId);
        if(memberOption.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }

        int pageNum = 0;
        if (page != null) {
            pageNum = page - 1;
        }
        if (pageNum < 0) {
            pageNum = 0;
        }
        Pageable pageable = PageRequest.of(pageNum, 25, Sort.Direction.ASC, "modTime");
        Page<ReqApply> getResults = reqApplyRepository.findByRequest_MemberUser_IdAndState(memberId, ReqAplyState.WAITING, pageable);

        return new PageImpl(
                getResults.getContent().stream()
                        .map(RequestApplyInfo::fromEntity)
                        .collect(Collectors.toList()),
                pageable, getResults.getTotalElements());
    }

	/*
	 * 사용자가 의뢰한 의뢰에대한 지원자 리스트 조회
	 */
    public Page<RequestApplyInfo> findApplyHelperList(Integer page, Long helperId, int[] states) {
        Optional<MemberHelper> findHelper = memberHelperRepository.findById(helperId);
        if(findHelper.isEmpty()) {
            throw new NotFoundException("헬퍼 정보를 찾을 수 없습니다.");
        }

        EnumSet<ReqAplyState> stateSet = EnumSet.noneOf(ReqAplyState.class);
        if(states != null) {
            for (int code: states) {
                ReqAplyState state = ReqAplyState.fromCode(code);
                if(state != null) {
                    stateSet.add(state);
                }
            }
        }
        if(stateSet.size() == 0) {
            stateSet = EnumSet.allOf(ReqAplyState.class);
        }

        int pageNum = 0;
        if (page != null) {
            pageNum = page - 1;
        }
        if (pageNum < 0) {
            pageNum = 0;
        }
        Pageable pageable = PageRequest.of(pageNum, 25, Sort.Direction.DESC, "modTime");
        Page<ReqApply> getResults = reqApplyRepository.findByMemberHelper_IdAndStateIn(helperId, stateSet, pageable);

        return new PageImpl(
                getResults.getContent().stream()
                        .map(RequestApplyInfo::fromEntity)
                        .collect(Collectors.toList()),
                pageable, getResults.getTotalElements());
    }
}
