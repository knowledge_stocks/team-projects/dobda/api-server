package com.dobda.apiserver.api.requestboard.entity;

import com.dobda.apiserver.api.member.user.entity.MemberUser;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(RequestReportKey.class)
@AllArgsConstructor
@Builder
public class RequestReport {
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "request_id", columnDefinition = "BIGINT UNSIGNED", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Request request;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id", columnDefinition = "INT UNSIGNED", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private MemberUser memberUser;
}
