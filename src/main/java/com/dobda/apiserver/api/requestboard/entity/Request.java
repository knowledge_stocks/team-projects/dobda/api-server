package com.dobda.apiserver.api.requestboard.entity;

import com.dobda.apiserver.api.member.user.entity.MemberUser;
import com.dobda.apiserver.common.enums.RequestCategory;
import com.dobda.apiserver.common.enums.RequestState;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@DynamicInsert
public class Request {
		
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "BIGINT UNSIGNED")
	private BigInteger id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "writer_id")
	private MemberUser memberUser;

    // 제목
    @Size(max=50)
    @Column(nullable = false)
    private String title;

    // 카테고리
    @Column(nullable = false, columnDefinition = "INT UNSIGNED")
    private RequestCategory category;

    // 의뢰 내용
    @Column(nullable = false, columnDefinition = "MEDIUMBLOB")
    private String description;

    // 희망 보수
    @Column(nullable = false, columnDefinition = "INT UNSIGNED")
    private Long payment;

    // 흥정 가능 여부
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private Boolean isCanSuggestPayment;

    // 의뢰 마감시간
    @Column(columnDefinition = "DATETIME")
    private ZonedDateTime deadline;

    // 의뢰 생성일
    @CreatedDate
    @Column(columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP", nullable = false)
    private ZonedDateTime regTime;

    // 의뢰 수정일
    @Column(columnDefinition = "DATETIME")
    private ZonedDateTime modTime;

    // 정렬 기준. 끌어올리기 시 현재 시간으로 변경
    @Column(columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP", nullable = false)
    private ZonedDateTime sortTime;

    // 조회수
    @Column(nullable = false, columnDefinition = "INT UNSIGNED DEFAULT 0")
    private Long viewCount;

    // 신고 받은 횟수
    @Column(nullable = false, columnDefinition = "INT UNSIGNED DEFAULT 0")
    private Long reportCount;

    // 관리자가 신고 무시한 게시글
    @Column(nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
    private Boolean ignoreReport = false;

    // 의뢰 상태
    @Column(nullable = false, columnDefinition = "TINYINT UNSIGNED")
    private RequestState state = RequestState.POSTED;

    // 기본 주소
    @Size(max=128)
    @Column(nullable = false)
    private String address1;

    // 상세 주소
    @Size(max=128)
    private String address2;

    // 시도 코드
    @Size(max=2)
    @Column(nullable = false)
    private String sidoCode;

    // 시군구 코드
    @Size(max=5)
    @Column(nullable = false)
    private String sigunguCode;

    // 법정동 코드
    @Size(max=10)
    @Column(nullable = false)
    private String dongCode;

    // 경도
    @Column(name = "axis_x", nullable = false)
    private Double axisX;

    // 위도
    @Column(name = "axis_y", nullable = false)
    private Double axisY;

    @Builder
    public Request(MemberUser memberUser, String title, RequestCategory category, String description,
                   Long payment, Boolean isCanSuggestPayment, ZonedDateTime deadline,
                   String address1, String address2, String sidoCode, String sigunguCode, String dongCode,
                   Double axisX, Double axisY) {
        this.memberUser = memberUser;
        this.title = title;
        this.category = category;
        this.description = description;
        this.payment = payment;
        this.isCanSuggestPayment = isCanSuggestPayment;
        this.deadline = deadline;
        this.address1 = address1;
        this.address2 = address2;
        this.sidoCode = sidoCode;
        this.sigunguCode = sigunguCode;
        this.dongCode = dongCode;
        this.axisX = axisX;
        this.axisY = axisY;
    }

    @OneToMany(mappedBy = "request")
    private List<ReqApply> reqApplyList = new ArrayList<>();
}