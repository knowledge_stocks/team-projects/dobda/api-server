package com.dobda.apiserver.api.requestboard.repository;

import com.dobda.apiserver.api.requestboard.entity.Request;
import com.dobda.apiserver.common.enums.RequestCategory;
import com.dobda.apiserver.common.enums.RequestState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigInteger;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;

public interface RequestRepository extends JpaRepository<Request, BigInteger>{

    @Query("FROM Request r " +
            "WHERE " +
            "(coalesce(:states, '') = '' OR r.state IN :states) " +
            "AND (coalesce(:categories, '') = '' OR r.category IN :categories) " +
            "AND (:minPrice IS NULL OR r.payment >= :minPrice) " +
            "AND (:maxPrice IS NULL OR r.payment <= :maxPrice) " +
            "AND (:keyword IS NULL OR r.title LIKE CONCAT('%',:keyword,'%')) " +
            "AND (:startX IS NULL OR r.axisX > :startX) " +
            "AND (:startY IS NULL OR r.axisY > :startY) " +
            "AND (:endX IS NULL OR r.axisX < :endX) " +
            "AND (:endY IS NULL OR r.axisY < :endY)")
    public Page<Request> findByQuery(
            @Param("states") EnumSet<RequestState> states,
            @Param("categories") EnumSet<RequestCategory> categories,
            @Param("minPrice") Long minPrice,
            @Param("maxPrice") Long maxPrice,
            @Param("keyword") String keyword,
            @Param("startX") Double startX,
            @Param("startY") Double startY,
            @Param("endX") Double endX,
            @Param("endY") Double endY,
            Pageable pageable);

    public Page<Request> findByMemberUser_IdAndStateIn(Long memberId, Collection<RequestState> states, Pageable pageable);

    public Page<Request> findByIgnoreReportFalseAndReportCountGreaterThanEqualAndStateIn(Long reportCount, Collection<RequestState> states, Pageable pageable);

	public List<Request> findByTitleContainingIgnoreCase(String title);
	
	List<Request> findByMemberUserContaining(Long memberUser);
	
	public List<Request> findById(Long id);
		
}
