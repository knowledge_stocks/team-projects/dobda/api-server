package com.dobda.apiserver.api.requestboard.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigInteger;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RequestReportKey implements Serializable {
    private BigInteger request;
    private Long memberUser;
}
