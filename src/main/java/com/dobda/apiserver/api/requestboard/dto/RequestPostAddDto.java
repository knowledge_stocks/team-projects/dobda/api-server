package com.dobda.apiserver.api.requestboard.dto;

import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;

@Data
@Builder
public class RequestPostAddDto {

	@Size(min = 1) private String requestTitle;
    @Size(min = 1) private String description;
    @NotNull private Integer category;
    @NotNull private Long payment;
    @NotNull private Boolean isCanSuggestPayment;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) private ZonedDateTime deadline;
    @Size(min = 1) private String address1;
    private String address2;
    @Size(min = 2, max = 2) private String sidoCode;
    @Size(min = 5, max = 5) private String sigunguCode;
    @Size(min = 10, max = 10) private String dongCode;
    @NotNull private Double axisX;
    @NotNull private Double axisY;
}
