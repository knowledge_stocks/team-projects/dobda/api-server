package com.dobda.apiserver.api.requestboard.exception;

public class RequestNotFoundException extends RuntimeException {
	
	public RequestNotFoundException() { super(); }
	
	public RequestNotFoundException(String message) {
		super(message);
	}
	
	public RequestNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public RequestNotFoundException(Throwable cause) {
		super(cause);
	}
}
