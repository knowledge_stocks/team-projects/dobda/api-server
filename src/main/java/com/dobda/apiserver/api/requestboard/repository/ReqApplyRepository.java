package com.dobda.apiserver.api.requestboard.repository;

import com.dobda.apiserver.api.requestboard.entity.ReqApply;
import com.dobda.apiserver.common.enums.ReqAplyState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.Collection;

public interface ReqApplyRepository extends JpaRepository<ReqApply, BigInteger>{

    public ReqApply findByMemberHelper_IdAndRequest_Id(Long helperId, BigInteger requestId);

    public Page<ReqApply> findByRequest_IdAndState(BigInteger requestId, ReqAplyState state, Pageable pageable);

	public Page<ReqApply> findByMemberHelper_IdNotAndRequest_IdAndState(Long helperId, BigInteger requestId, ReqAplyState state, Pageable pageable);

	public Page<ReqApply> findByMemberHelper_IdAndStateIn(Long memberId, Collection<ReqAplyState> states, Pageable pageable);

    public Page<ReqApply> findByRequest_MemberUser_IdAndState(Long helperId, ReqAplyState state, Pageable pageable);
}
