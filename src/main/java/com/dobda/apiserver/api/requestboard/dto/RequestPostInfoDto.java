package com.dobda.apiserver.api.requestboard.dto;

import com.dobda.apiserver.api.member.user.dto.MemberPublicInfo;
import com.dobda.apiserver.api.requestboard.entity.Request;
import com.dobda.apiserver.common.enums.RequestCategory;
import com.dobda.apiserver.common.enums.RequestState;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigInteger;
import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
@Builder
public class RequestPostInfoDto {
	private BigInteger requestId;
	private MemberPublicInfo writerInfo;
	private String title;
    private RequestCategory category;
	private String desscription;
    private Long payment;
    private Boolean isCanSuggestPayment;
    private ZonedDateTime deadline;
    private ZonedDateTime regTime;
    private ZonedDateTime modTime;
    private Long viewCount;
    private Long reportCount;
    private RequestState state;
	private String address1;
	private String address2;
    private String sidoCode;
    private String sigunguCode;
    private String dongCode;
    private Double axisX;
    private Double axisY;
		
	public static RequestPostInfoDto fromEntity(Request entity) {
        if(entity == null) {
            return null;
        }
		return builder()
				.requestId(entity.getId())
				.writerInfo(MemberPublicInfo.fromEntity(entity.getMemberUser()))
				.title(entity.getTitle())
                .category(entity.getCategory())
				.desscription(entity.getDescription())
                .payment(entity.getPayment())
                .isCanSuggestPayment(entity.getIsCanSuggestPayment())
                .deadline(entity.getDeadline())
                .regTime(entity.getRegTime())
                .modTime(entity.getModTime())
                .viewCount(entity.getViewCount())
                .reportCount(entity.getReportCount())
                .state(entity.getState())
				.address1(entity.getAddress1())
				.address2(entity.getAddress2())
                .sidoCode(entity.getSidoCode())
                .sigunguCode(entity.getSigunguCode())
                .dongCode(entity.getDongCode())
                .axisX(entity.getAxisX())
                .axisY(entity.getAxisY())
				.build();
	}
}
