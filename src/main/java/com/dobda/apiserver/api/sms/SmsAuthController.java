package com.dobda.apiserver.api.sms;

import com.dobda.apiserver.api.common.errorhandling.BaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SmsAuthController {

    @Autowired SmsAuthService smsAuthService;

    @PostMapping("/api/public/requestAuthSms")
    public ResponseEntity genAuthSms(
            @RequestParam("phoneNum") String phoneNum
    ) {
        try {
            String key = smsAuthService.sendAuthSms(phoneNum);
            return new ResponseEntity(key, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @PostMapping("/api/public/requestAuthSms/check")
    public ResponseEntity checkAuthSms(
            @RequestParam("phoneNum") String phoneNum,
            @RequestParam("keyNum") String keyNum
    ) {
        try {
            smsAuthService.checkAuthSms(phoneNum, keyNum);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @PostMapping("/api/public/requestReAuthSms")
    public ResponseEntity genReAuthSms(
            @RequestParam("phoneNum") String phoneNum
    ) {
        try {
            String key = smsAuthService.sendReAuthSms(phoneNum);
            return new ResponseEntity(key, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @PostMapping("/api/public/requestReAuthSms/check")
    public ResponseEntity checkReAuthSms(
            @RequestParam("phoneNum") String phoneNum,
            @RequestParam("keyNum") String keyNum
    ) {
        try {
            smsAuthService.checkReAuthSms(phoneNum, keyNum);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }
}
