package com.dobda.apiserver.api.member.user.dto;

import com.dobda.apiserver.common.enums.ResourceType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class AddNotificationMemberDto {

    @NotNull
    private Long receiverId;

    @NotNull
    private String message;

    private ResourceType targetType;
    private BigInteger targetId;
}
