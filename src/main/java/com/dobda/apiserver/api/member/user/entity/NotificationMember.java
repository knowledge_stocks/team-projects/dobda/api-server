package com.dobda.apiserver.api.member.user.entity;

import com.dobda.apiserver.common.enums.ResourceType;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigInteger;
import java.time.ZonedDateTime;

@Entity
@Getter @Setter
@NoArgsConstructor
@DynamicInsert
public class NotificationMember {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "BIGINT UNSIGNED")
	private BigInteger id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "member_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private MemberUser memberUser;

	@Size(max = 200)
	@Column(nullable = false)
	private String message;

	@Column(columnDefinition = "TINYINT UNSIGNED", nullable = false)
	private ResourceType targetType;				//알람이 발생한 항목의 타입

	@Column(columnDefinition = "BIGINT UNSIGNED", nullable = false)
	private BigInteger targetId;					//알람이 발생한 항목의 id

	@CreatedDate
	@Column(columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP", nullable = false)
	private ZonedDateTime regTime;					//알람 발생일시

	@Builder
	public NotificationMember(MemberUser memberUser, String message,
                              ResourceType targetType, BigInteger targetId) {
		this.memberUser = memberUser;
		this.message = message;
		this.targetType = targetType;
		this.targetId = targetId;
	}
	
	@PostPersist
	public void postPersist() {
		int newNotiCount = memberUser.getUnreadNotiCount() + 1;
		if(newNotiCount > 100) {
			newNotiCount = 100;
		}
        memberUser.setUnreadNotiCount(newNotiCount);
	}
}
