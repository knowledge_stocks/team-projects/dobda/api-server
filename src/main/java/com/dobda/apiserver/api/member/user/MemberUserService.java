package com.dobda.apiserver.api.member.user;

import com.dobda.apiserver.api.common.errorhandling.*;
import com.dobda.apiserver.api.member.user.dto.*;
import com.dobda.apiserver.api.member.user.entity.MemberUser;
import com.dobda.apiserver.api.member.user.entity.NotificationMember;
import com.dobda.apiserver.api.order.OrderRepository;
import com.dobda.apiserver.api.order.entity.Order;
import com.dobda.apiserver.api.sms.SmsAuthKeyRepository;
import com.dobda.apiserver.api.sms.entity.SmsAuthKey;
import com.dobda.apiserver.common.enums.OrderState;
import com.dobda.apiserver.common.enums.SmsAuthKeyState;
import com.dobda.apiserver.common.util.openbank.OpenBankUtils;
import com.dobda.apiserver.common.util.openbank.token.UserTokenRequest;
import com.dobda.apiserver.common.util.openbank.token.UserTokenResponse;
import com.dobda.apiserver.security.authprovider.PasswordUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.*;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service("memberUserService")
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class MemberUserService implements UserDetailsService {

	private final MemberUserRepository memberUserRepository;
	private final SmsAuthKeyRepository smsAuthKeyRepository;
	private final NotificationMemberRepository notificationMemberRepository;
    private final OrderRepository orderRepository;
	private final EntityManager em;

//	@Qualifier("refreshTokenTaskExecutor")
//	private final TaskExecutor refreshTokenTaskExecutor;

	@Transactional
	public void join(MemberUserJoinDTO dto) {
		if (!dto.getEmail().matches("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$")) {
			throw new BadRequestException("이메일 형식이 올바르지 않습니다.");
		}
		// 개발용 주석
        if(!dto.getPwd().matches("^(?=.*[a-zA-Z])(?=.*[\\d])(?=.*[!@#$%^&_*])[a-zA-Z\\d!@#$%^&_*]{8,30}$")) {
            throw new BadRequestException("비밀번호는 8~30자 이내 영문,숫자,특수문자를 한번씩 조합해야 합니다.");
        }
		if (!dto.getNickname().matches("^\\p{L}[\\p{L}\\d]{2,19}$")) {
			throw new BadRequestException("닉네임은 3~20자 이내 일반문자, 숫자로만 구성되어야 합니다.");
		}

		Optional<SmsAuthKey> findAuthKey = smsAuthKeyRepository.findById(dto.getPhoneNum().replace("-", ""));
		if (findAuthKey.isEmpty()) {
			throw new BadRequestException("SMS 인증정보가 없습니다. 다시 인증해주세요.");
		}

		SmsAuthKey authKey = findAuthKey.get();
		if (authKey.getExpireTime().isBefore(ZonedDateTime.now())) {
			throw new BadRequestException("SMS 인증정보가 만료되었습니다. 다시 인증해주세요.");
		}
		if (authKey.getState() != SmsAuthKeyState.AUTHENTICATED) {
			throw new BadRequestException("발급받은 SMS 인증키를 먼저 인증해주세요.");
		}
		if (!authKey.getKeyNum().equals(dto.getPhoneAuthKey())) {
			throw new BadRequestException("SMS 인증키가 일치하지 않습니다.");
		}
        smsAuthKeyRepository.delete(authKey);

        String bankToken = null;
        String bankRefreshToken = null;
        ZonedDateTime bankTokenExpireTime = null;
        String bankUserCode = null;
        if(dto.getBankAuthCode() != null) {
            UserTokenResponse tokenResponse = OpenBankUtils.getUserToken(
                    UserTokenRequest.builder().code(dto.getBankAuthCode()).redirectUri(dto.getBankCallbackUri()).build());

            if (tokenResponse.getAccessToken() == null) {
                throw new BadRequestException("오픈뱅킹 인증 정보가 올바르지 않습니다.");
            }

            bankToken = tokenResponse.getAccessToken();
            bankRefreshToken = tokenResponse.getRefreshToken();
            bankTokenExpireTime = ZonedDateTime.now().plusSeconds(tokenResponse.getExpiresIn());
            bankUserCode = tokenResponse.getUserSeqNo();
        }

		MemberUser user = MemberUser.builder().email(dto.getEmail()).pwd(new byte[0]).name(dto.getName())
				.nickname(dto.getNickname()).phoneNum(authKey.getPhoneNum()).introduction(dto.getIntroduction())
				.bankToken(bankToken).bankRefreshToken(bankRefreshToken)
				.bankTokenExpireTime(bankTokenExpireTime)
				.bankUserCode(bankUserCode).build();

		memberUserRepository.save(user);
		em.clear();
		user = memberUserRepository.findById(user.getId()).get();

		byte[] encryptPassword = new byte[0];
		try {
			encryptPassword = PasswordUtil.genDigest(dto.getPwd(), user.getRegTime());
		} catch (NoSuchAlgorithmException e) {
			throw new InternalServerErrorException(e.getMessage());
		}
		user.setPwd(encryptPassword);
		smsAuthKeyRepository.delete(authKey);
	}

    @Transactional
    public void reAuthBank(Long memberId, MemberUserReBankAuthDTO dto) {
        Optional<MemberUser> findUser = memberUserRepository.findById(memberId);
        if (findUser.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberUser user = findUser.get();
        try {
            byte[] checkDigest = PasswordUtil.genDigest(dto.getPwd(), user.getRegTime());
            if (!PasswordUtil.compare(checkDigest, user.getPwd())) {
                throw new UnauthorizedException("비밀번호가 일치하지 않습니다.");
            }
        } catch (NoSuchAlgorithmException e) {
            throw new InternalServerErrorException(e);
        }

        UserTokenResponse tokenResponse = OpenBankUtils.getUserToken(
                UserTokenRequest.builder()
                        .code(dto.getBankAuthCode())
                        .redirectUri(dto.getBankCallbackUri())
                        .build());

        if (tokenResponse.getAccessToken() == null) {
            throw new BadRequestException("오픈뱅킹 인증 정보가 올바르지 않습니다.");
        }

        user.setBankToken(tokenResponse.getAccessToken());
        user.setBankRefreshToken(tokenResponse.getRefreshToken());
        user.setBankTokenExpireTime(ZonedDateTime.now().plusSeconds(tokenResponse.getExpiresIn()));
        user.setBankUserCode(tokenResponse.getUserSeqNo());
    }

	public boolean emailDuplicate(String email) {
		MemberUser findUser = memberUserRepository.findByEmail(email);

		if (findUser != null) {
			log.info("findEmails: " + findUser.getEmail());
			return false;
		} else {
			return true;
		}
	}

	public boolean nickNameDuplicate(String nickName) {
		MemberUser findUser = memberUserRepository.findByNickname(nickName);

		if (findUser != null) {
			log.info("findNickNames: " + findUser.getNickname());
			return false;
		} else {
			return true;
		}
	}

	public boolean phoneNumDuplicate(String phoneNum) {
		MemberUser findUser = memberUserRepository.findByPhoneNum(phoneNum);

		if (findUser != null) {
			log.info("findPhoneNum: " + findUser.getPhoneNum());
			return false;
		} else {
			return true;
		}
	}

	public MemberDetailInfo getMemberDetailInfo(long id) {
		Optional<MemberUser> findUser = memberUserRepository.findById(id);
		if (findUser.isEmpty()) {
			throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
		}
		return MemberDetailInfo.fromEntity(findUser.get());
	}

	public MemberPublicInfo getMemberPublicInfo(long id) {
		Optional<MemberUser> findUser = memberUserRepository.findById(id);
		if (findUser.isEmpty()) {
			throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
		}
		return MemberPublicInfo.fromEntity(findUser.get());
	}

	@Transactional
	public void removeMember(Long id, String pwd) {
		Optional<MemberUser> findUser = memberUserRepository.findById(id);
		if (findUser.isEmpty()) {
			return;
		}
		MemberUser user = findUser.get();
        byte[] checkDigest = null;
        try {
            checkDigest = PasswordUtil.genDigest(pwd, user.getRegTime());
        } catch (NoSuchAlgorithmException e) {
            throw new InternalServerErrorException(e);
        }
		if (!PasswordUtil.compare(checkDigest, user.getPwd())) {
			throw new UnauthorizedException("비밀번호가 일치하지 않습니다.");
		}

        EnumSet enumSet = EnumSet.of(OrderState.WAITING, OrderState.ACCEPTED, OrderState.WAITING_REFUND);
        List<Order> requesterOrders = orderRepository.findAllByMemberUser_IdAndStateIn(id, enumSet);
        if(requesterOrders != null && !requesterOrders.isEmpty()) {
            throw new BadRequestException("아직 진행중인 주문이 남아 있습니다.");
        }
        enumSet = EnumSet.of(OrderState.WAITING, OrderState.ACCEPTED, OrderState.WAITING_CALC);
        List<Order> helpersOrder = orderRepository.findAllByMemberHelper_IdAndStateIn(id, enumSet);
        if(helpersOrder != null && !helpersOrder.isEmpty()) {
            throw new BadRequestException("아직 진행중인 주문이 남아 있습니다.");
        }

		String bankToken = user.getBankToken();
		memberUserRepository.deleteById(id);
        if(bankToken != null) {
            OpenBankUtils.revokeToken(bankToken);
        }
	}

	@Override
	public MemberUser loadUserByUsername(String email) throws UsernameNotFoundException {
		return memberUserRepository.findByEmail(email);
	}

    // 알림 관련 API들
    @Transactional
    public void checkRead(Long memberId) {
        Optional<MemberUser> findResult = memberUserRepository.findById(memberId);
        if(findResult.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberUser memberUser = findResult.get();
        memberUser.setUnreadNotiCount(0);
    }

    @Transactional
    public NotificationMemberInfoDto addNoti(Long memberId, AddNotificationMemberDto data) {
        Optional<MemberUser> findUser = memberUserRepository.findById(memberId);
        if(findUser.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberUser memberUser = findUser.get();

        NotificationMember newNoti = NotificationMember.builder()
                .memberUser(memberUser)
                .message(data.getMessage())
                .targetType(data.getTargetType())
                .targetId(data.getTargetId())
                .build();
        newNoti.setRegTime(ZonedDateTime.now());

        notificationMemberRepository.save(newNoti);

        return NotificationMemberInfoDto.fromEntity(newNoti);
    }

    @Transactional
    public Page<NotificationMemberInfoDto> getNotiList(Long memberId, BigInteger offsetId) {
        Optional<MemberUser> findUser = memberUserRepository.findById(memberId);
        if(findUser.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberUser memberUser = findUser.get();
        memberUser.setUnreadNotiCount(0);

        Pageable pageable = PageRequest.of(0, 20, Sort.Direction.DESC, "id");
        Page<NotificationMember> findResults = null;
        if(offsetId == null || offsetId.equals("")) {
            findResults = notificationMemberRepository.findByMemberUser_Id(memberId, pageable);
        } else {
            findResults = notificationMemberRepository.findByIdLessThanAndMemberUser_Id(offsetId, memberId, pageable);
        }

        return new PageImpl(
                findResults.getContent().stream()
                        .map(NotificationMemberInfoDto::fromEntity)
                        .collect(Collectors.toList()),
                pageable, findResults.getTotalElements());
    }

    @Transactional
    public void clearNotiList(Long memberId) {
        Optional<MemberUser> findUser = memberUserRepository.findById(memberId);
        if(findUser.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberUser memberUser = findUser.get();
        memberUser.setUnreadNotiCount(0);

        notificationMemberRepository.deleteByMemberUser_Id(memberId);
    }

    @Transactional
    public void removeNoti(Long memberId, BigInteger notiId) {
        Optional<NotificationMember> findNoti = notificationMemberRepository.findById(notiId);
        if(findNoti.isEmpty()) {
            throw new NotFoundException("항목을 찾을 수 없습니다.");
        }
        NotificationMember noti = findNoti.get();
        if(noti.getMemberUser() == null || !noti.getMemberUser().getId().equals(memberId)) {
            throw new ForbiddenException("권한이 없습니다.");
        }

        notificationMemberRepository.delete(noti);
    }

    // 매일 자정에 곧 만료될 토큰들 갱신 // 사용자가 수동으로 갱신하도록 하는 걸로.
//    @Scheduled(cron = "0 0 0 * * *")
//    public void refreshExpiredBankToken() {
//        List<MemberUser> bankTokenExpiredUsers = memberUserRepository.findByBankTokenExpireTimeBefore(ZonedDateTime.now().plusDays(1));
//
//        for (MemberUser user: bankTokenExpiredUsers) {
//            refreshTokenTaskExecutor.execute(new BankTokenRefreshThread(user, memberUserRepository));
//        }
//    }

	//////////////////////////////////// 사용자 정보 변경
	//////////////////////////////////// API///////////////////////////////////////////////////
	/*
	 * 비밀번호 변경
	 */
	@Transactional
	public void changePw(Long memberId, String newPw, String curPw) {
        if(!newPw.matches("^(?=.*[a-zA-Z])(?=.*[\\d])(?=.*[!@#$%^&_*])[a-zA-Z\\d!@#$%^&_*]{8,30}$")) {
            throw new BadRequestException("비밀번호는 8~30자 이내 영문,숫자,특수문자를 한번씩 조합해야 합니다.");
        }

		Optional<MemberUser> findUser = memberUserRepository.findById(memberId);
        if(findUser.isEmpty()) {
            throw new UnauthorizedException("사용자 정보를 찾을 수 없습니다.");
        }
		MemberUser user = findUser.get();

        try {
            byte[] currentPassword = PasswordUtil.genDigest(curPw, user.getRegTime());
            if (!PasswordUtil.compare(currentPassword, user.getPwd())) {
                throw new UnauthorizedException("비밀번호가 일치하지 않습니다.");
            }
            log.info("Pw 동일 합니다.");

            byte[] newPassword = PasswordUtil.genDigest(newPw, user.getRegTime());
            user.setPwd(newPassword);

            log.info("Pw 변경되었습니다.");
        } catch (NoSuchAlgorithmException e) {
            throw new InternalServerErrorException(e);
        }
	}
	/*
	 * 이메일 변경
	 * */
	@Transactional
	public void changeEmail(Long memberId, String email) {
		log.info("memberId: " + memberId);
		log.info("email: " + email);

		Optional<MemberUser> findUser = memberUserRepository.findById(memberId);
        if(findUser.isEmpty()) {
            throw new UnauthorizedException("사용자 정보를 찾을 수 없습니다.");
        }
		MemberUser user = findUser.get();

        if (emailDuplicate(email)) {
            log.info("사용가능한 이메일 입니다.");
            user.setEmail(email);
        } else {
            log.info("email is duplicated");
            throw new BadRequestException("이미 사용중인 이메일 입니다.");
        }
		log.info("이메일 변경이 완료되었습니다.");
	}

	/*
	 * 휴대폰 번호 변경
	 */
	@Transactional
	public void changePhoneNumber(Long memberId, String phoneNumber, String key, String pwd) {
        phoneNumber = phoneNumber.replace("-", "");
		log.info("phoneNumber: " + phoneNumber);

		Optional<MemberUser> findUser = memberUserRepository.findById(memberId);
		MemberUser user = findUser.get();
        try {
            byte[] checkDigest = PasswordUtil.genDigest(pwd, user.getRegTime());
            if (!PasswordUtil.compare(checkDigest, user.getPwd())) {
                throw new UnauthorizedException("비밀번호가 일치하지 않습니다.");
            }
        } catch (NoSuchAlgorithmException e) {
            throw new InternalServerErrorException(e);
        }

        Optional<SmsAuthKey> findAuthKey = smsAuthKeyRepository.findById(phoneNumber);
        if (findAuthKey.isEmpty()) {
            throw new BadRequestException("SMS 인증정보가 없습니다. 다시 인증해주세요.");
        }
        SmsAuthKey authKey = findAuthKey.get();
        if (authKey.getExpireTime().isBefore(ZonedDateTime.now())) {
            throw new BadRequestException("SMS 인증정보가 만료되었습니다. 다시 인증해주세요.");
        }
        if (authKey.getState() != SmsAuthKeyState.RE_AUTHENTICATED) {
            throw new BadRequestException("발급받은 SMS 인증키를 먼저 인증해주세요.");
        }
        if (!authKey.getKeyNum().equals(key)) {
            throw new BadRequestException("SMS 인증키가 일치하지 않습니다.");
        }
        smsAuthKeyRepository.delete(authKey);
        user.setPhoneNum(phoneNumber);
	}

	/*
	 * 닉네임 변경
	 */
	@Transactional
	public void changeNickName(Long memberId, String newNickName) {
		log.info("memberId: " + memberId);
		log.info("newNickName: " + newNickName);
        Optional<MemberUser> findUser = memberUserRepository.findById(memberId);
        if(findUser.isEmpty()) {
            throw new UnauthorizedException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberUser user = findUser.get();

        if (nickNameDuplicate(newNickName)) {
            log.info("사용가능한 닉네임 입니다.");
            user.setNickname(newNickName);
        } else {
            log.info("nickname is duplicated");
            throw new BadRequestException("중복되는 닉네임 입니다.");
        }
        log.info("닉네임 변경이 완료되었습니다.");
	}

	/*
	 * 자기소개 변경
	 */
	@Transactional
	public void changeIntroduction(Long memberId, String intro) {
		log.info("memberId: " + memberId);
		log.info("intro: " + intro);
        Optional<MemberUser> findUser = memberUserRepository.findById(memberId);
        if(findUser.isEmpty()) {
            throw new UnauthorizedException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberUser user = findUser.get();

		user.setIntroduction(intro);
		log.info("소개글 변경 완료하였습니다.");
	}
}
