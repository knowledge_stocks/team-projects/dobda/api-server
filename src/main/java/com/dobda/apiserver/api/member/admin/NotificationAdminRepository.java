package com.dobda.apiserver.api.member.admin;

import com.dobda.apiserver.api.member.admin.entity.NotificationAdmin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;

public interface NotificationAdminRepository extends JpaRepository<NotificationAdmin, BigInteger> {

    public Page<NotificationAdmin> findByMemberAdmin_Id(Long adminId, Pageable pageable);

    public void deleteByMemberAdmin_Id(Long adminId);

    public Page<NotificationAdmin> findByIdLessThanAndMemberAdmin_Id(BigInteger offsetId, Long adminId, Pageable pageable);
}
