package com.dobda.apiserver.api.member.admin.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class MemberAdminAddDTO {
	
	@NotNull
    private String email;
    
    @NotNull
    private String pwd;
    
    @NotNull 
    private String name;

    @NotNull
    private String nickname;
    
    @NotNull
    private String phoneNum;
}
