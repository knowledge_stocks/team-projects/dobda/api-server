package com.dobda.apiserver.api.member.admin.dto;

import com.dobda.apiserver.common.enums.ResourceType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class AddNotificationAdminDto {

    @NotNull
    private Long adminId;

    @NotNull
    private String message;

    private Long sourceMemberId;
    private ResourceType targetType;
    private BigInteger targetId;
}
