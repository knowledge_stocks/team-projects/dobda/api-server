package com.dobda.apiserver.api.member.admin.entity;

import com.dobda.apiserver.common.enums.AdminLevel;
import com.dobda.apiserver.security.MemberRoles;
import com.dobda.apiserver.security.model.CommonMemberSecureInfo;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Getter @Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class MemberAdmin extends CommonMemberSecureInfo{

	@Size(min=2, max=50)
	@Column(nullable = false)
	private String name;

    @Size(min=3, max=50)
    @Column(unique = true, nullable = false)
    private String nickname;
	
	@Size(max=20)
	@Column(unique = true, nullable = false)
	private String phoneNum;
	
	@Column(columnDefinition = "TINYINT UNSIGNED", nullable = false)
	private AdminLevel level = AdminLevel.JUNIOR;
	
	@Column(columnDefinition = "TINYINT UNSIGNED DEFAULT 0", nullable = false)
	private Integer unreadNotiCount;

	@OneToMany(mappedBy = "memberAdmin", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<NotificationAdmin> notificationList = new ArrayList<>();
	
    @Builder
    public MemberAdmin(String email, byte[] pwd, String name, String nickname, String phoneNum) {
        super(email, pwd);
        this.name = name;
        this.nickname = nickname;
        this.phoneNum = phoneNum;
    }

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(MemberRoles.ADMIN.getFullRole()));
        if(level == AdminLevel.MASTER) {
            authorities.add(new SimpleGrantedAuthority(AdminLevel.MASTER.toString()));
        }
		return authorities;
	}
}
