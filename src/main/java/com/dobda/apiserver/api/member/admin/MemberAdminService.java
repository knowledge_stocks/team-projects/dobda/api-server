package com.dobda.apiserver.api.member.admin;

import com.dobda.apiserver.api.common.errorhandling.*;
import com.dobda.apiserver.api.member.admin.dto.*;
import com.dobda.apiserver.api.member.admin.entity.MemberAdmin;
import com.dobda.apiserver.api.member.admin.entity.NotificationAdmin;
import com.dobda.apiserver.common.enums.AdminLevel;
import com.dobda.apiserver.security.authprovider.PasswordUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service("memberAdminService")
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class MemberAdminService implements UserDetailsService{
	
	private final MemberAdminRepository memberAdminRepository;
    private final NotificationAdminRepository notificationAdminRepository;
    private final EntityManager em;
	
	@Transactional
	public void add(Authentication authentication, MemberAdminAddDTO dto) {
        boolean isFirstAdmin = true;
        if(memberAdminRepository.count() > 0) {
            isFirstAdmin = false;
            if(authentication == null || authentication.getAuthorities().stream()
                    .filter(auth -> auth.getAuthority().equals(AdminLevel.MASTER.toString()))
                    .findAny().isEmpty()) {
                throw new ForbiddenException("권한이 없습니다.");
            }
        }

        if(!dto.getEmail().matches("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$")) {
            throw new BadRequestException("이메일 형식이 올바르지 않습니다.");
        }
        // 개발용 주석
        if(!dto.getPwd().matches("^(?=.*[a-zA-Z])(?=.*[\\d])(?=.*[!@#$%^&_*])[a-zA-Z\\d!@#$%^&_*]{8,30}$")) {
            throw new BadRequestException("비밀번호는 8~30자 이내 영문,숫자,특수문자를 한번씩 조합해야 합니다.");
        }
        if(!dto.getNickname().matches("^\\p{L}[\\p{L}\\d]{2,19}$")) {
            throw new BadRequestException("닉네임은 3~20자 이내 일반문자, 숫자로만 구성되어야 합니다.");
        }

        MemberAdmin newAdmin = MemberAdmin.builder()
                .email(dto.getEmail())
                .pwd(new byte[0])
                .name(dto.getName())
                .nickname(dto.getNickname())
                .phoneNum(dto.getPhoneNum().replace("-", ""))
                .build();

        memberAdminRepository.save(newAdmin);
        em.clear();
        newAdmin = memberAdminRepository.findById(newAdmin.getId()).get();

        byte[] encryptPassword = new byte[0];
        try {
            encryptPassword = PasswordUtil.genDigest(dto.getPwd(), newAdmin.getRegTime());
        } catch (NoSuchAlgorithmException e) {
            throw new InternalServerErrorException(e.getMessage());
        }
        newAdmin.setPwd(encryptPassword);
        if(isFirstAdmin) {
            newAdmin.setLevel(AdminLevel.MASTER);
        }
	}

    public boolean emailDuplicate(String email) {
		MemberAdmin findUser = memberAdminRepository.findByEmail(email);

		if(findUser != null) {
			log.info("findEmails: " + findUser.getEmail());
			return false;
		}else{return true;}
	}
	
	public boolean nickNameDuplicate(String nickName) {
        MemberAdmin findUser = memberAdminRepository.findByNickname(nickName);

        if(findUser != null) {
			log.info("findNickNames: " + findUser.getNickname());
			return false;
		}else {return true;}
	}

    public boolean phoneNumDuplicate(String phoneNum) {
        MemberAdmin findUser = memberAdminRepository.findByPhoneNum(phoneNum);

        if(findUser != null) {
            log.info("findPhoneNum: " + findUser.getPhoneNum());
            return false;
        }else {return true;}
    }

    public MemberAdminDetailInfo getMemberDetailInfo(long id) {
        Optional<MemberAdmin> findUser = memberAdminRepository.findById(id);
        if(findUser.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        return MemberAdminDetailInfo.fromEntity(findUser.get());
    }

    public MemberAdminPublicInfo getMemberPublicInfo(long id) {
        Optional<MemberAdmin> findUser = memberAdminRepository.findById(id);
        if(findUser.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        return MemberAdminPublicInfo.fromEntity(findUser.get());
    }

    @Transactional
    public void removeMember(Long id, String pwd)  {
        Optional<MemberAdmin> findUser = memberAdminRepository.findById(id);
        if(findUser.isEmpty()) {
            return;
        }
        MemberAdmin user = findUser.get();
        if(user.getLevel() == AdminLevel.MASTER) {
            throw new BadRequestException("마스터 계정은 제거할 수 없습니다.");
        }
        byte[] checkDigest = null;
        try {
            checkDigest = PasswordUtil.genDigest(pwd, user.getRegTime());
        } catch (NoSuchAlgorithmException e) {
            throw new InternalServerErrorException(e);
        }
        if(!PasswordUtil.compare(checkDigest, user.getPwd())) {
            throw new UnauthorizedException("비밀번호가 일치하지 않습니다.");
        }
        memberAdminRepository.deleteById(id);
    }
    
    //관리자 비밀번호 변경 
    @Transactional
    public void changePwd(Long id, String pwd, String newPwd) {
        if(!newPwd.matches("^(?=.*[a-zA-Z])(?=.*[\\d])(?=.*[!@#$%^&_*])[a-zA-Z\\d!@#$%^&_*]{8,30}$")) {
            throw new BadRequestException("비밀번호는 8~30자 이내 영문,숫자,특수문자를 한번씩 조합해야 합니다.");
        }

    	Optional<MemberAdmin> findAdmin = memberAdminRepository.findById(id);
        if(findAdmin.isEmpty()) {
            throw new UnauthorizedException("관리자 정보를 찾을 수 없습니다.");
        }
        MemberAdmin admin = findAdmin.get();

        try {
            byte[] currentPassword = PasswordUtil.genDigest(pwd, admin.getRegTime());
            if (!PasswordUtil.compare(currentPassword, admin.getPwd())) {
                throw new UnauthorizedException("비밀번호가 일치하지 않습니다.");
            }
            log.info("Pw 동일 합니다.");

            byte[] newPassword = PasswordUtil.genDigest(newPwd, admin.getRegTime());
            admin.setPwd(newPassword);

            log.info("Pw 변경되었습니다.");
        } catch (NoSuchAlgorithmException e) {
            throw new InternalServerErrorException(e);
        }
    }

    /*
     * 이메일 변경
     * */
    @Transactional
    public void changeEmail(Long adminId, String email) {
        Optional<MemberAdmin> findAdmin = memberAdminRepository.findById(adminId);
        if(findAdmin.isEmpty()) {
            throw new UnauthorizedException("관리자 정보를 찾을 수 없습니다.");
        }
        MemberAdmin admin = findAdmin.get();

        if (emailDuplicate(email)) {
            admin.setEmail(email);
        } else {
            throw new BadRequestException("이미 사용중인 이메일 입니다.");
        }
    }

    /*
     * 휴대폰 번호 변경
     */
    @Transactional
    public void changePhoneNumber(Long adminId, String phoneNumber, String pwd) {
        phoneNumber = phoneNumber.replace("-", "");

        Optional<MemberAdmin> findAdmin = memberAdminRepository.findById(adminId);
        if(findAdmin.isEmpty()) {
            throw new UnauthorizedException("관리자 정보를 찾을 수 없습니다.");
        }
        MemberAdmin admin = findAdmin.get();
        try {
            byte[] checkDigest = PasswordUtil.genDigest(pwd, admin.getRegTime());
            if (!PasswordUtil.compare(checkDigest, admin.getPwd())) {
                throw new UnauthorizedException("비밀번호가 일치하지 않습니다.");
            }
        } catch (NoSuchAlgorithmException e) {
            throw new InternalServerErrorException(e);
        }

        if (phoneNumDuplicate(phoneNumber)) {
            admin.setPhoneNum(phoneNumber);
        } else {
            throw new BadRequestException("이미 사용중인 전화번호 입니다.");
        }
    }

    /*
     * 닉네임 변경
     */
    @Transactional
    public void changeNickName(Long adminId, String newNickName) {
        Optional<MemberAdmin> findAdmin = memberAdminRepository.findById(adminId);
        if(findAdmin.isEmpty()) {
            throw new UnauthorizedException("관리자 정보를 찾을 수 없습니다.");
        }
        MemberAdmin admin = findAdmin.get();

        if (nickNameDuplicate(newNickName)) {
            admin.setNickname(newNickName);
        } else {
            throw new BadRequestException("중복되는 닉네임 입니다.");
        }
    }

    
    // 알림 관련 API들
    @Transactional
    public void checkRead(Long memberId) {
        Optional<MemberAdmin> findResult = memberAdminRepository.findById(memberId);
        if(findResult.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberAdmin member = findResult.get();
        member.setUnreadNotiCount(0);
    }

    @Transactional
    public NotificationAdminInfoDto addNoti(Long memberId, AddNotificationAdminDto data) {
        Optional<MemberAdmin> findMember = memberAdminRepository.findById(memberId);
        if(findMember.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberAdmin memberAdmin = findMember.get();

        NotificationAdmin newNoti = NotificationAdmin.builder()
                .memberAdmin(memberAdmin)
                .message(data.getMessage())
                .sourceMemberId(data.getSourceMemberId())
                .targetType(data.getTargetType())
                .targetId(data.getTargetId())
                .build();
        newNoti.setRegTime(ZonedDateTime.now());

        notificationAdminRepository.save(newNoti);

        return NotificationAdminInfoDto.fromEntity(newNoti);
    }

    @Transactional
    public Page<NotificationAdminInfoDto> getNotiList(Long memberId, BigInteger offsetId) {
        Optional<MemberAdmin> findMember = memberAdminRepository.findById(memberId);
        if(findMember.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberAdmin memberAdmin = findMember.get();
        memberAdmin.setUnreadNotiCount(0);

        Pageable pageable = PageRequest.of(0, 20, Sort.Direction.DESC, "id");
        Page<NotificationAdmin> findResults = null;
        if(offsetId == null || offsetId.equals("")) {
            findResults = notificationAdminRepository.findByMemberAdmin_Id(memberId, pageable);
        } else {
            findResults = notificationAdminRepository.findByIdLessThanAndMemberAdmin_Id(offsetId, memberId, pageable);
        }

        return new PageImpl(
                findResults.getContent().stream()
                        .map(NotificationAdminInfoDto::fromEntity)
                        .collect(Collectors.toList()),
                pageable, findResults.getTotalElements());
    }

    @Transactional
    public void clearNotiList(Long memberId) {
        Optional<MemberAdmin> findUser = memberAdminRepository.findById(memberId);
        if(findUser.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberAdmin memberAdmin = findUser.get();
        memberAdmin.setUnreadNotiCount(0);

        notificationAdminRepository.deleteByMemberAdmin_Id(memberId);
    }

    @Transactional
    public void removeNoti(Long memberId, BigInteger notiId) {
        Optional<NotificationAdmin> findNoti = notificationAdminRepository.findById(notiId);
        if(findNoti.isEmpty()) {
            throw new NotFoundException("항목을 찾을 수 없습니다.");
        }
        NotificationAdmin noti = findNoti.get();
        if(noti.getMemberAdmin() == null || !noti.getMemberAdmin().getId().equals(memberId)) {
            throw new ForbiddenException("권한이 없습니다.");
        }

        notificationAdminRepository.delete(noti);
    }

	@Override
	public MemberAdmin loadUserByUsername(String email) throws UsernameNotFoundException {
		return memberAdminRepository.findByEmail(email);
	}

}
