package com.dobda.apiserver.api.member.helper.dto;

import com.dobda.apiserver.api.member.helper.entity.HelperApply;
import com.dobda.apiserver.api.member.user.dto.MemberPublicInfo;
import com.dobda.apiserver.common.enums.HelperApplyState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@Builder
@AllArgsConstructor
public class HelperApplyInfo {
	private Long id;

    private MemberPublicInfo memberInfo;
	
	private String address;
	
	private String idCard;
	
	private String bankName;
	
	private String fintechNum;
	
	private HelperApplyState state;
	
	private ZonedDateTime regTime;
	
	public static HelperApplyInfo fromEntity(HelperApply entity) {
		if(entity == null) {return null;}
		
		return builder()
				.id(entity.getId())
                .memberInfo(MemberPublicInfo.fromEntity(entity.getMemberUser()))
				.address(entity.getAddress())
				.idCard("/admin/upload/idCard/" + entity.getId())
				.bankName(entity.getBankName())
				.fintechNum(entity.getFintechNum())
				.state(entity.getState())
				.regTime(entity.getRegTime())
				.build();
	}
}
