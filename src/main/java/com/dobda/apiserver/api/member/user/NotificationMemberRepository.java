package com.dobda.apiserver.api.member.user;

import com.dobda.apiserver.api.member.user.entity.NotificationMember;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;

public interface NotificationMemberRepository extends JpaRepository<NotificationMember, BigInteger> {

    public Page<NotificationMember> findByMemberUser_Id(Long memberId, Pageable pageable);

    public void deleteByMemberUser_Id(Long memberId);

    public Page<NotificationMember> findByIdLessThanAndMemberUser_Id(BigInteger offsetId, Long memberId, Pageable pageable);
}
