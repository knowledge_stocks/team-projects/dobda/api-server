package com.dobda.apiserver.api.member.user.dto;

import com.dobda.apiserver.api.member.helper.dto.MemberHelperInfoDto;
import com.dobda.apiserver.api.member.user.entity.MemberUser;
import com.dobda.apiserver.common.enums.MemberUserLevel;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@Builder
public class MemberDetailInfo {
    private Boolean isAdmin;
    private Long id;
    private String email;
    private String name;
    private String nickname;
    private String phoneNum;
    private Boolean isHelper;
    private String introduction;
    private Integer unreadNotiCount;
    private Boolean hasUnreadChat;
    private ZonedDateTime regTime;
    private ZonedDateTime denyTime;
    private Double trustPoint;
    private Long totalRequestCount;
    private Long totalReceivedReportCount;
    private MemberHelperInfoDto helperInfo;

    public static MemberDetailInfo fromEntity(MemberUser entity) {
        if (entity == null) {
            return null;
        }
        return builder()
                .isAdmin(false)
                .id(entity.getId())
                .email(entity.getEmail())
                .name(entity.getName())
                .nickname(entity.getNickname())
                .phoneNum(entity.getPhoneNum())
                .isHelper(entity.getLevel() == MemberUserLevel.HELPER)
                .introduction(entity.getIntroduction())
                .unreadNotiCount(entity.getUnreadNotiCount())
                .hasUnreadChat(entity.getChattingRoomList().stream()
                        .anyMatch(v -> v.getHasUnreadMsg()))
                .regTime(entity.getRegTime())
                .denyTime(entity.getDenyTime())
                .trustPoint(entity.getTrustPoint())
                .totalRequestCount(entity.getTotalRequestCount())
                .totalReceivedReportCount(entity.getTotalReceivedReportCount())
                .helperInfo(MemberHelperInfoDto.fromEntity(entity.getMemberHelper()))
                .build();
    }
}
