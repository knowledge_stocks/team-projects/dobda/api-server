package com.dobda.apiserver.api.member.user;

import com.dobda.apiserver.api.member.user.entity.MemberUser;
import com.dobda.apiserver.common.util.openbank.OpenBankUtils;
import com.dobda.apiserver.common.util.openbank.token.RefreshTokenRequest;
import com.dobda.apiserver.common.util.openbank.token.RefreshTokenResponse;

import java.time.ZonedDateTime;

public class BankTokenRefreshThread implements Runnable {

    private MemberUser user;

    private MemberUserRepository memberUserRepository;

    public BankTokenRefreshThread(MemberUser user, MemberUserRepository memberUserRepository) {
        this.user = user;
        this.memberUserRepository = memberUserRepository;
    }

    @Override
    public void run() {
        RefreshTokenResponse tokenResponse = OpenBankUtils.refreshUserToken(RefreshTokenRequest.builder()
                .refreshToken(user.getBankRefreshToken())
                .scope("login transfer")
                .build());
        if(tokenResponse.getAccessToken() == null) {
            return;
        }

        user.setBankToken(tokenResponse.getAccessToken());
        user.setBankRefreshToken(tokenResponse.getRefreshToken());
        user.setBankTokenExpireTime(ZonedDateTime.now().plusSeconds(tokenResponse.getExpiresIn()));
        user.setBankUserCode(tokenResponse.getUserSeqNo());
        memberUserRepository.save(user);

        // 트랜젝션이 필요할 때 참고
//        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
//            @Override
//            protected void doInTransactionWithoutResult(TransactionStatus status) {
//            }
//        });
    }
}
