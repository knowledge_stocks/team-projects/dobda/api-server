package com.dobda.apiserver.api.member.helper.entity;

import com.dobda.apiserver.api.member.user.entity.MemberUser;
import com.dobda.apiserver.common.enums.HelperApplyState;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@DynamicInsert
public class HelperApply {
    @Id
    private Long id;

    @MapsId
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id", columnDefinition = "INT UNSIGNED")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private MemberUser memberUser;

    @Size(max=128)
    @Column(nullable = false)
    private String address;
    
    @Size(max = 50)
    @Column(nullable = false)
    private String bankName;

    @Size(max=24)
    @Column(nullable = false)
    private String fintechNum;

    @Column(nullable = false, columnDefinition = "TINYINT UNSIGNED")
    private HelperApplyState state = HelperApplyState.WAITING;

    @CreatedDate
    @Column(columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP", nullable = false)
    private ZonedDateTime regTime;
    
    @Builder
    public HelperApply(MemberUser memberUser, String address, String bankName, String fintechNum) {
        this.memberUser = memberUser;
        this.address = address;
        this.bankName = bankName;
        this.fintechNum = fintechNum;
    }
}
