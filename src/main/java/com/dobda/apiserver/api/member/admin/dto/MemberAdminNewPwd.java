package com.dobda.apiserver.api.member.admin.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class MemberAdminNewPwd {
	@NotNull
    @Size(min=1)
	private String pwd;
	
	@NotNull
    @Size(min=1)
	private String newPwd;

}
