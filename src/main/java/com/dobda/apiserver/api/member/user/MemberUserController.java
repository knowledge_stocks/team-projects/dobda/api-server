package com.dobda.apiserver.api.member.user;

import com.dobda.apiserver.api.common.errorhandling.BaseException;
import com.dobda.apiserver.api.member.user.dto.MemberPublicInfo;
import com.dobda.apiserver.api.member.user.dto.MemberUserJoinDTO;
import com.dobda.apiserver.api.member.user.dto.MemberUserReBankAuthDTO;
import com.dobda.apiserver.api.member.user.dto.NotificationMemberInfoDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.math.BigInteger;


@RestController
@Slf4j
@Validated
public class MemberUserController {

	@Autowired
	private MemberUserService memberUserService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;


	/*
	 * 일반 회원가입
	 */
	@PostMapping("/api/userJoin")
	public ResponseEntity userJoin(@ModelAttribute @Valid MemberUserJoinDTO dto) {
		log.info("userJoin Controller 진입");

        try {
            memberUserService.join(dto);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
	}

    @PutMapping("/api/user/bankReAuth")
    public ResponseEntity bankReAuth(
            Authentication authentication,
            @ModelAttribute MemberUserReBankAuthDTO dto
    ) {
        try {
            memberUserService.reAuthBank((Long) authentication.getPrincipal(), dto);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

	/*
	 * 가입 시 이메일 중복체크
	 */
	@GetMapping("/api/public/emailChk")
	public ResponseEntity<Boolean> emailDuplicateChk(@RequestParam("email") String email) {
		log.info("email: " + email);
		if(email.isEmpty() || email.trim().equals("")) {
			//log.info("email: " +email.isEmpty());
			return new ResponseEntity("이메일은 필수 입력 사항입니다.", HttpStatus.BAD_REQUEST);
		}

		boolean emailChk = memberUserService.emailDuplicate(email);
		return ResponseEntity.ok(emailChk);
	}

	/*
	 * 가입 시 닉네임 중복체크
	 */
	@GetMapping("/api/public/nicknameChk")
	public ResponseEntity<Boolean> nickNameDuplicateChk(@RequestParam("nickname") String nickname) {
		log.info("nickname: " + nickname);
		if(nickname.isEmpty() || nickname.trim().equals("")) {
			return new ResponseEntity("닉네임은 필수 입력 사항입니다.", HttpStatus.BAD_REQUEST);
		}

		boolean nickNameChk = memberUserService.nickNameDuplicate(nickname);
		return ResponseEntity.ok(nickNameChk);
	}

    @DeleteMapping("/api/user")
    public ResponseEntity changeInfo(
            Authentication authentication,
            @RequestHeader("pwd") String pwd
    ) {
        try {
            memberUserService.removeMember((Long) authentication.getPrincipal(), pwd);
            SecurityContextHolder.getContext().setAuthentication(null);
            SecurityContextHolder.clearContext();
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @GetMapping("/api/public/user/{id}")
    public ResponseEntity info(@PathVariable("id") Long id) {
        try {
            MemberPublicInfo result = memberUserService.getMemberPublicInfo(id);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @PutMapping("/api/user/noti/read")
    public ResponseEntity checkRead(
            Authentication authentication) {
        try {
            memberUserService.checkRead((Long) authentication.getPrincipal());
            simpMessagingTemplate.convertAndSend(
                    "/ws/topic/" + authentication.getPrincipal() + "/readnoti", "");
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @GetMapping("/api/user/noti/list")
    public ResponseEntity getNotiList(
            Authentication authentication,
            @RequestParam(value = "offsetId", required = false) BigInteger offsetId
    ) {
        try {
            Page<NotificationMemberInfoDto> result =
                    memberUserService.getNotiList((Long) authentication.getPrincipal(), offsetId);
            simpMessagingTemplate.convertAndSend(
                    "/ws/topic/" + authentication.getPrincipal() + "/readnoti", "");
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @DeleteMapping("/api/user/noti")
    public ResponseEntity clearNoti(
            Authentication authentication
    ) {
        try {
            memberUserService.clearNotiList((Long)authentication.getPrincipal());
            simpMessagingTemplate.convertAndSend(
                    "/ws/topic/" + authentication.getPrincipal() + "/readnoti", "");
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @DeleteMapping("/api/user/noti/{id}")
    public ResponseEntity removeNoti(
            Authentication authentication,
            @PathVariable("id") BigInteger id
    ) {
        try {
            memberUserService.removeNoti((Long)authentication.getPrincipal(),id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    ////////////////////////////////////사용자 정보 변경 API///////////////////////////////////////////////////

    /*
     * 비밀변호 변경
     * 비밀번호 변경시 고려사항
     * 1. 현제 비밀번호가 같은지?
     * 2. 변경한 비밀번호가 같은지?
     * */
    @PutMapping("/api/user/pwChange")
    public ResponseEntity changeUserPw(Authentication authentication,
    		@RequestParam("newPw") String newPw,
    		@RequestParam("oldPw") String oldPw) {
    	// 비밀번호 변경 요청이 들어 왔으므로, 현제 비밀번호가 맞는지 확인
    	log.info("비밀번호 확인 요청");
        try {
            memberUserService.changePw((Long)authentication.getPrincipal(), newPw, oldPw);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    /*
     * 이메일 변경
     * */
    @PutMapping("/api/user/emailChange")
    public ResponseEntity changeUserEmail(
            Authentication authentication,
            @RequestParam("email") @Size(min = 3) @Valid String email
    ) {
        try {
            memberUserService.changeEmail((Long)authentication.getPrincipal(), email);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    /*
     * 휴대폰 번호 변경
     * 변경전 인증 절차 필요
     * */
    @PutMapping("/api/user/phoneNumberChange")
    public ResponseEntity changeUserPhoneNumber(
            Authentication authentication,
            @RequestParam("phoneNum") String phoneNum,
            @RequestParam("key") String key,
            @RequestParam("pwd") String pwd
    ) {
        try {
            memberUserService.changePhoneNumber((Long)authentication.getPrincipal(), phoneNum, key, pwd);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    /*
     * 닉네임 변겅
     * 닉네임 중복 확인후 변경
     * */
    @PutMapping("/api/user/nickChange")
    public ResponseEntity changeUserNickName(Authentication authentication,
    		@RequestParam("newNickName") @Size(min = 3) @Valid String newNickName){
        try {
            memberUserService.changeNickName((Long)authentication.getPrincipal(), newNickName);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    /*
     * 자기소개 변경
     * */
    @PutMapping("/api/user/introChange")
    public ResponseEntity changeUserIntro(Authentication authentication, @RequestParam("intro")String intro) {
        try {
            memberUserService.changeIntroduction((Long)authentication.getPrincipal(), intro);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }
}
