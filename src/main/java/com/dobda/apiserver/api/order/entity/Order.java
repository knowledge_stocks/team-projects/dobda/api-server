package com.dobda.apiserver.api.order.entity;

import com.dobda.apiserver.api.member.helper.entity.MemberHelper;
import com.dobda.apiserver.api.member.user.entity.MemberUser;
import com.dobda.apiserver.api.requestboard.entity.Request;
import com.dobda.apiserver.api.review.entity.Review;
import com.dobda.apiserver.common.enums.OrderState;
import com.dobda.apiserver.common.enums.PaymentMethod;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@DynamicInsert
@Table(name = "`order`")
public class Order {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "BIGINT UNSIGNED")
    private BigInteger id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "requester_id")
    private MemberUser memberUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "helper_id")
    private MemberHelper memberHelper;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "request_id", nullable = false)
    private Request request;

    @Size(max=24)
    private String requesterFintech;
    
    @Size(max=50)
    private String requesterBankName;

    @Column(nullable = false, columnDefinition = "TINYINT UNSIGNED")
	private PaymentMethod payMethod;

    @Column(nullable = false, columnDefinition = "INT UNSIGNED")
	private Long amount;

    @CreatedDate
    @Column(columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP", nullable = false)
    private ZonedDateTime regTime;

    @Column(nullable = false, columnDefinition = "TINYINT UNSIGNED")
	private OrderState state = OrderState.WAITING;

    @Builder
    public Order(MemberUser memberUser, MemberHelper memberHelper, Request request,
    		String requesterFintech, String requesterBankName, PaymentMethod payMethod, Long amount) {
        this.memberUser = memberUser;
        this.memberHelper = memberHelper;
        this.request = request;
        this.requesterFintech = requesterFintech;
        this.requesterBankName = requesterBankName;
        this.payMethod = payMethod;
        this.amount = amount;
    }

    @OneToMany(mappedBy = "order")
    private List<Review> reviewList = new ArrayList<>();
}
