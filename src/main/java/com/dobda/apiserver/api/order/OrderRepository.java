package com.dobda.apiserver.api.order;

import com.dobda.apiserver.api.order.entity.Order;
import com.dobda.apiserver.common.enums.OrderState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

public interface OrderRepository extends JpaRepository<Order, BigInteger>{

    Page<Order> findByMemberUser_IdAndStateIn(Long memberId, Collection<OrderState> states, Pageable pageable);

    Page<Order> findByMemberHelper_IdAndStateIn(Long helperId, Collection<OrderState> states, Pageable pageable);

    List<Order> findAllByMemberUser_IdAndStateIn(Long memberId, Collection<OrderState> states);

    List<Order> findAllByMemberHelper_IdAndStateIn(Long helperId, Collection<OrderState> states);

    List<Order> findAllByMemberUser_IdAndState(Long memberId, OrderState state);

    List<Order> findAllByMemberHelper_IdAndState(Long helperId, OrderState state);
}
