package com.dobda.apiserver.api.order.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RetryRefundAllDto {
    @NotNull private String bankName;
    @NotNull private String fintech;
}
