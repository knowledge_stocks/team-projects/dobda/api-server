package com.dobda.apiserver.api.chat.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class SendChattingMsgDto {

    @NotNull
    private Long receiverId;

    @NotNull
    private String message;
}
