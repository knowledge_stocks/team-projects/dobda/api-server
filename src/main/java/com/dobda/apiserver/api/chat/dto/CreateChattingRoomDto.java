package com.dobda.apiserver.api.chat.dto;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class CreateChattingRoomDto {

    @NotNull
    private Long partnerId;
}
