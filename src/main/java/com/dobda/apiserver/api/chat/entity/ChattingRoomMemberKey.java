package com.dobda.apiserver.api.chat.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigInteger;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChattingRoomMemberKey implements Serializable {
    private BigInteger chattingRoom;
    private Long memberUser;
}
