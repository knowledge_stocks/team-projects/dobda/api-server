package com.dobda.apiserver.api.chat;

import com.dobda.apiserver.api.chat.dto.ChattingMsgInfoDto;
import com.dobda.apiserver.api.chat.dto.ChattingRoomInfoDto;
import com.dobda.apiserver.api.chat.dto.CreateChattingRoomDto;
import com.dobda.apiserver.api.chat.dto.SendChattingMsgDto;
import com.dobda.apiserver.api.common.errorhandling.BaseException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class ChattingController {

    private final ChattingService chattingService;
    private final SimpMessagingTemplate simpMessagingTemplate;

    @PostMapping("/user/chatting/room")
    public ResponseEntity createRoom(
            Authentication authentication,
            @ModelAttribute CreateChattingRoomDto data) {
        try {
            ChattingRoomInfoDto result = chattingService.createRoom((Long) authentication.getPrincipal(), data);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @GetMapping("/user/chatting/room/list")
    public ResponseEntity getChattingRoomList(
            Authentication authentication,
            @PageableDefault(size=20, sort="chattingRoom.lastTime", direction = Sort.Direction.DESC, page = 0) Pageable pageable) {
        try {
            Page<ChattingRoomInfoDto> result =
                    chattingService.getChattingRoomList((Long) authentication.getPrincipal(), pageable);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @PutMapping("/user/chatting/room/{id}/read")
    public ResponseEntity checkRead(
            Authentication authentication,
            @PathVariable("id") BigInteger id) {
        try {
            chattingService.checkRead((Long) authentication.getPrincipal(), id);
            simpMessagingTemplate.convertAndSend(
                    "/ws/topic/" + authentication.getPrincipal() + "/readchat/" + id, id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @MessageMapping("/user/chat")
    public ResponseEntity sendMsg(
            Authentication authentication,
            @ModelAttribute SendChattingMsgDto data) {
        try {
            ChattingMsgInfoDto result = chattingService.sendMsg((Long) authentication.getPrincipal(), data);
            simpMessagingTemplate.convertAndSend(
                    "/ws/topic/" + data.getReceiverId() + "/chat/" + result.getRoomId(),
                    result);
            simpMessagingTemplate.convertAndSend(
                    "/ws/topic/" + authentication.getPrincipal() + "/chat/" + result.getRoomId(),
                    result);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @GetMapping("/user/chatting/msg/list")
    public ResponseEntity getChattingMsgList(
            Authentication authentication,
            @RequestParam(value = "offsetId", required = false) BigInteger offsetId,
            @RequestParam("roomId") BigInteger roomId
    ) {
        try {
            Page<ChattingMsgInfoDto> result =
                    chattingService.getChattingMsgList((Long) authentication.getPrincipal(), roomId, offsetId);
            simpMessagingTemplate.convertAndSend(
                    "/ws/topic/" + authentication.getPrincipal() + "/readchat/" + roomId, roomId);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }
}
