package com.dobda.apiserver.api.chat;

import com.dobda.apiserver.api.chat.entity.ChattingRoomMember;
import com.dobda.apiserver.api.chat.entity.ChattingRoomMemberKey;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChattingRoomMemberRepository extends JpaRepository<ChattingRoomMember, ChattingRoomMemberKey> {

    public ChattingRoomMember findByMemberUser_IdAndPartner_Id(Long memberId, Long partnerId);

    public Page<ChattingRoomMember> findByMemberUser_Id(Long memberId, Pageable pageable);
}
