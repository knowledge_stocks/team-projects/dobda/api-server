package com.dobda.apiserver.api.payment;

import com.dobda.apiserver.api.common.errorhandling.NotFoundException;
import com.dobda.apiserver.api.common.errorhandling.UnauthorizedException;
import com.dobda.apiserver.api.member.user.MemberUserRepository;
import com.dobda.apiserver.api.member.user.entity.MemberUser;
import com.dobda.apiserver.api.payment.dto.MyAccountsInfo;
import com.dobda.apiserver.common.util.openbank.OpenBankUtils;
import com.dobda.apiserver.common.util.openbank.account.AccountResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
@Transactional(readOnly = true)
public class PaymentService {

    private final MemberUserRepository memberUserRepository;

	// 사용자 계좌 조회 //
	public MyAccountsInfo getMyAccountsInfo(Long memberId) {
        Optional<MemberUser> findUser = memberUserRepository.findById(memberId);
        if(findUser.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberUser memberUser = findUser.get();

        if(memberUser.getBankToken() == null) {
            throw new UnauthorizedException("오픈뱅킹 인증이 필요합니다. 인증 정보를 등록해주세요");
        }
        AccountResponse response = OpenBankUtils.getMyInfo(memberUser.getBankToken(), memberUser.getBankUserCode());
        if(!response.getRsp_code().equals("A0000")) {
            throw new UnauthorizedException("인증 토큰이 유효하지 않습니다. 다시 인증해주세요");
        }

        return MyAccountsInfo.of(response);
	}
}
