package com.dobda.apiserver.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum OrderState {
    WAITING(0), // 결제 후 헬퍼의 승락 대기(현금 결제일 경우 결제 전이어도 이 상태)
    ACCEPTED(10), // 헬퍼가 승락
//    REQUESTER_CANCEL(20), // ACCEPTED에서 의뢰자의 취소 요청. 헬퍼가 수락해야 함
//    HELPER_CANCEL(30), // ACCEPTED에서 헬퍼의 취소 요청. 의뢰자가 수락해야 함
    COMPLETE(40), // 의뢰자가 완료 버튼 클릭. 대금을 헬퍼 계좌에 입금
    CANCELED(50), // 취소됨. 환불 진행
    WAITING_REFUND(60), // 의뢰자에게 입금 실패시
    WAITING_CALC(70); // 헬퍼로 입금 실패시

    private static final Map<Integer, OrderState> intToEnum =
            Arrays.stream(values()).collect(Collectors.toMap(OrderState::getCode, e -> e));

    @Getter
    @JsonValue
    private final Integer code;

    private OrderState(int code)
    {
        this.code = code;
    }

    public static OrderState fromCode(int code) {
        OrderState result = intToEnum.get(code);
        return result;
    }
}
