package com.dobda.apiserver.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum ReqAplyState {
    WAITING(10), // 지원 대기중
    REJECTED(20), // 거부됨
    SELECTED(30); // 선택됨

    private static final Map<Integer, ReqAplyState> intToEnum =
            Arrays.stream(values()).collect(Collectors.toMap(ReqAplyState::getCode, e -> e));

    @Getter
    @JsonValue
    private final Integer code;

    private ReqAplyState(int code)
    {
        this.code = code;
    }

    public static ReqAplyState fromCode(int code) {
        ReqAplyState result = intToEnum.get(code);
        return result;
    }
}
