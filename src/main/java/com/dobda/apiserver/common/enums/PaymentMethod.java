package com.dobda.apiserver.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum PaymentMethod {
    CASH(1), // 현금
    ACCOUNT_TRANSFER(2); // 계좌 이체

    private static final Map<Integer, PaymentMethod> intToEnum =
            Arrays.stream(values()).collect(Collectors.toMap(PaymentMethod::getCode, e -> e));

    @Getter
    @JsonValue
    private final Integer code;

    private PaymentMethod(int code)
    {
        this.code = code;
    }

    public static PaymentMethod fromCode(int code) {
        PaymentMethod result = intToEnum.get(code);
        return result;
    }
}
