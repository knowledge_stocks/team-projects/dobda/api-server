package com.dobda.apiserver.common.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class AdminLevelConverter implements AttributeConverter<AdminLevel, Integer> {
    @Override
    public Integer convertToDatabaseColumn(AdminLevel enumValue) {
        if (enumValue == null) {
            return null;
        }
        return enumValue.getCode();
    }

    @Override
    public AdminLevel convertToEntityAttribute(Integer integer) {
        if(integer == null) {
            return null;
        }
        return AdminLevel.fromCode(integer);
    }
}
