package com.dobda.apiserver.common.util.openbank.payment;

import java.util.List;

import lombok.Data;

@Data
public class DepositResponse {
	
	private String api_tran_id;
	private String api_tran_dtm;
	private String rsp_code;
	private String rsp_message;
	private String wd_bank_code_std;
	private String wd_bank_name;
	private String wd_account_num_masked;
	private String wd_print_content;
	private String wd_account_holder_name;
	private String res_cnt;
	private List<Details> res_list;
	
	@Data
	public static class Details{
		private String tran_no;
		private String bank_tran_id;
		private String bank_tran_date;
		private String bank_code_tran;
		private String bank_rsp_code;
		private String bank_res_message;
		private String fintech_use_num;
		private String account_alias;
		private String bank_code_std;
		private String bank_code_sub;
		private String bank_name;
		private String savings_bank_name;
		private String account_num_masked;
		private String print_content;
		private String account_holder_name;
		private String tran_amt;
		private String cms_num;
	}

}
