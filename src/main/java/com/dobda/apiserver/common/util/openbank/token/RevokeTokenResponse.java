package com.dobda.apiserver.common.util.openbank.token;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class RevokeTokenResponse {
    private String rspCode;
    private String rspMessage;
    private String clientId;
    private String clientSecret;
    private String accessToken;
    private String refreshToken;
}
