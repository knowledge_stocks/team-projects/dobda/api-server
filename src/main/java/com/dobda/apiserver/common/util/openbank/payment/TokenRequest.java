package com.dobda.apiserver.common.util.openbank.payment;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TokenRequest {

	private String client_id;
	private String client_secret;
	private String scope;
	private String grant_type;

}
