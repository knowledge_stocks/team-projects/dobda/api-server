package com.dobda.apiserver.common.util.openbank.token;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OrgTokenResponse {
    private String rspCode;
    private String rspMessage;
    private String accessToken;
    private String tokenType;
    private Long expiresIn;
    private String scope;
    private String clientUseCode;
}
