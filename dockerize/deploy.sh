#!/bin/sh
cd ~/dobda/backend

docker build -t dobda-backend . &&\
docker-compose down &&\
docker rmi $(docker images -f "dangling=true" -q) &&\
docker-compose up -d
